package be.kdg.pro12.Sale;

public class SalesLineItem {
	private Product product;
	private int quantity;

	public SalesLineItem(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "SalesLineItem{" +
			"product=" + product +
			", quantity=" + quantity +
			'}';
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
