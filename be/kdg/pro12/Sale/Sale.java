package be.kdg.pro12.Sale;

import java.util.ArrayList;
import java.util.List;

public class Sale {
	private List<SalesLineItem> lines = new ArrayList<>();

	public void addLine(SalesLineItem line) {
		lines.add(line);
	}

	@Override
	public String toString() {
		return "Sale{" +
			"with " + getNrOfSalesLines() +
			" lines: " + lines +
			'}';
	}

	public int getNrOfSalesLines() {
		return lines.size();
	}
}
