package be.kdg.pro12.Sale;

public class Product {
	private double price;
	private String description;
	private long id;
	private int vatPercentage;

	public Product( long id, String description, double price,int vatPercentage) {
		this.price = price;
		this.description = description;
		this.id = id;
		this.vatPercentage = vatPercentage;
	}

	@Override
	public String toString() {
		return "Product{" +
			"price=" + price +
			", description='" + description + '\'' +
			", id=" + id +
			", vatPercentage=" + vatPercentage +
			'}';
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVatPercentage() {
		return vatPercentage;
	}

	public void setVatPercentage(int vatPercentage) {
		this.vatPercentage = vatPercentage;
	}
}
