package be.kdg.pro12;

import be.kdg.pro12.Sale.*;

public class SalesRunner {
	public static void main(String[] args) {
		Product ketchup = new Product(1,"Heinz Tomato ketchup, 57 varieties", 1.63, 21);
		Product juice = new Product(2,"Appesientje", 1.13, 21);
		// price increase
		juice.setPrice(1.30);
		Sale wednesdayShopping = new Sale();
		wednesdayShopping.addLine(new SalesLineItem(ketchup,3));
		wednesdayShopping.addLine(new SalesLineItem(juice,1));
		System.out.println("I bought " + wednesdayShopping);
	}
}
